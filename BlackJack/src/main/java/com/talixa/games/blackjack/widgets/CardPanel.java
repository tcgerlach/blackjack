package com.talixa.games.blackjack.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JPanel;

import com.talixa.games.blackjack.data.Card;
import com.talixa.games.blackjack.data.Deck;
import com.talixa.games.blackjack.shared.BlackJackConstants;
import com.talixa.games.blackjack.shared.IconHelper;

@SuppressWarnings("serial")
public class CardPanel extends JPanel {

	private boolean soundOn = true;
	private Deck deck;
	private List<Card> playerPlayedCards;
	private List<Card> dealerPlayedCards;
	private boolean roundFinished = false;

	private static final int CARD_WIDTH = 100;
	private static final int CARD_HEIGHT = 150;
	private static final int CARD_PADDING = 10;
	private static final int DEALER_CARD_VERTICAL_TOP = 20;
	private static final int PLAYER_CARD_VERTICAL_TOP = BlackJackConstants.APP_HEIGHT - CARD_HEIGHT - 80;

	private static final int DECK_X = (BlackJackConstants.APP_WIDTH / 2) - (CARD_WIDTH / 2);
	private static final int DECK_Y = (BlackJackConstants.APP_HEIGHT / 2) - (CARD_HEIGHT / 2) - 25;

	private static final int HOLD_X = DECK_X + CARD_WIDTH + 15;
	private static final int HOLD_Y = DECK_Y + CARD_HEIGHT / 2;
	private static final int NEW_X = HOLD_X;
	private static final int NEW_Y = HOLD_Y - 45;

	private static final int HOLD_HEIGHT = 25;
	private static final int HOLD_WIDTH = 75;

	private static final int BORDER_RADIUS = 10;	

	private static Image diamond = IconHelper.getImage("res/diamonds.png");
	private static Image heart = IconHelper.getImage("res/hearts.png");
	private static Image club = IconHelper.getImage("res/clubs.png");
	private static Image spade = IconHelper.getImage("res/spades.png");
	private static Image back = IconHelper.getImage("res/cards.png");

	private static final int STARTING_CASH = 500;
	private static final int COST_PER_GAME = 10;
	private static final int WIN_PER_GAME = 50;

	private int money = STARTING_CASH;

	public CardPanel() {		
		reset(true);
	
		this.addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {
				// Deck clicked, add new card
				if (e.getX() > DECK_X && e.getX() < DECK_X + CARD_WIDTH && !roundFinished) {
					if (e.getY() > DECK_Y && e.getY() < DECK_Y + CARD_HEIGHT) {
						playerPlayedCards.add(deck.getTopCard());
						flipCard(playerPlayedCards.get(playerPlayedCards.size() - 1));
						if (getScore(playerPlayedCards) >= 21) {
							dealerTurn();
						}
						repaint();
					}
				}

				// hold hit, dealer turn
				if (e.getX() > HOLD_X && e.getX() < HOLD_X + HOLD_WIDTH) {
					if (e.getY() > HOLD_Y && e.getY() < HOLD_Y + HOLD_HEIGHT) {
						dealerTurn();
					}
				}

				// new game clicked - reset
				if (e.getX() > NEW_X && e.getX() < NEW_X + HOLD_WIDTH) {
					if (e.getY() > NEW_Y && e.getY() < NEW_Y + HOLD_HEIGHT) {
						reset(false);
					}
				}
			}

			public void mousePressed(MouseEvent e) {
				// DO NOTHING
			}

			public void mouseExited(MouseEvent e) {
				// DO NOTHING
			}

			public void mouseEntered(MouseEvent e) {
				// DO NOTHING
			}

			public void mouseClicked(MouseEvent e) {
				// DO NOTHING
			}
		});
	}

	public void reset(boolean resetMoney) {
		if (resetMoney) {
			money = STARTING_CASH;
		}
		money -= COST_PER_GAME;

		roundFinished = false;
		deck = new Deck();
		playerPlayedCards = new ArrayList<Card>();
		dealerPlayedCards = new ArrayList<Card>();
		
		deck.shuffle();
		
		// deal to player and dealer
		for(int i = 0; i < 2; ++i) {
			playerPlayedCards.add(deck.getTopCard());
			dealerPlayedCards.add(deck.getTopCard());
		}

		this.repaint();
		// create animation thread
		Runnable r = new Runnable() {
			public void run() {
				try {
					flipCard(dealerPlayedCards.get(0));
					repaint();
					Thread.sleep(500);

					flipCard(playerPlayedCards.get(0));
					repaint();
					Thread.sleep(500);

					flipCard(playerPlayedCards.get(1));			
					repaint();
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};

		// run animation
		Thread t = new Thread(r);
		t.start();
	}

	public void enableSound(boolean enabled) {
		this.soundOn = enabled;
	}

	private void playAudio(String resName) {
		if (soundOn) {
			ClassLoader cl = ClassLoader.getSystemClassLoader();
			URL resource = cl.getResource(resName);
			try {
				Clip clip = AudioSystem.getClip();
				AudioInputStream inputStream = AudioSystem.getAudioInputStream(resource);
    			clip.open(inputStream);
    			clip.start();
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		// board background color
		g.setColor(new Color(0x006600));
		g.fillRect(0, 0, BlackJackConstants.APP_WIDTH, BlackJackConstants.APP_HEIGHT);

		// draw dealer cards
		for(int i = 0; i < dealerPlayedCards.size(); ++i) {
			int myX = (CARD_PADDING*(i+1)) + (i*CARD_WIDTH);
			drawCard(g, dealerPlayedCards.get(i), myX, DEALER_CARD_VERTICAL_TOP);
		}

		// draw player cards
		for(int i = 0; i < playerPlayedCards.size(); ++i) {
			int myX = (CARD_PADDING*(i+1)) + (i*CARD_WIDTH);
			drawCard(g, playerPlayedCards.get(i), myX, PLAYER_CARD_VERTICAL_TOP);
		}

		// draw deck
		g.drawImage(back, DECK_X , DECK_Y, CARD_WIDTH, CARD_HEIGHT, null);

		// draw stay button
		g.setColor(Color.WHITE);
		g.fillRoundRect(HOLD_X, HOLD_Y , HOLD_WIDTH, HOLD_HEIGHT, BORDER_RADIUS, BORDER_RADIUS);
		g.setColor(Color.RED);
		g.drawRoundRect(HOLD_X, HOLD_Y , HOLD_WIDTH, HOLD_HEIGHT, BORDER_RADIUS, BORDER_RADIUS);
		g.drawString("Hold", HOLD_X + 25, HOLD_Y + 16);

		// draw new button
		if (roundFinished) {
			g.setColor(Color.WHITE);
			g.fillRoundRect(NEW_X, NEW_Y , HOLD_WIDTH, HOLD_HEIGHT, BORDER_RADIUS, BORDER_RADIUS);
			g.setColor(Color.RED);
			g.drawRoundRect(NEW_X, NEW_Y , HOLD_WIDTH, HOLD_HEIGHT, BORDER_RADIUS, BORDER_RADIUS);
			g.drawString("Deal", NEW_X + 25, NEW_Y + 16);
		}

		// draw info
		g.setColor(Color.BLACK);
		g.drawString("Money: $" + money, 10, 265);
		g.drawString("Dealer: " + getScore(dealerPlayedCards), 10, 285);
		g.drawString("Player: " + getScore(playerPlayedCards), 10, 305);
		if (roundFinished) {
			if (didPlayerWin()) {
				g.drawString("Winner: Player!", 10, 335);
			} else {
				g.drawString("Winner: Dealer", 10, 335);
			}
		}
	}

	private void drawCard(Graphics g, Card c, int x, int y) {	
		if (c.isFaceUp()) {
			// draw card color
			g.setColor(Color.white);
			g.fillRoundRect(x, y, CARD_WIDTH, CARD_HEIGHT, BORDER_RADIUS, BORDER_RADIUS);

			// set font color
			if (c.isRed()) {
				g.setColor(Color.RED);
			} else {
				g.setColor(Color.BLACK);
			}
			g.drawString(c.getAbbreviatedValue(), x + 4, y + 16);
			g.drawString(c.getAbbreviatedValue(), x + CARD_WIDTH - 14, y + CARD_HEIGHT - 8);

			// draw suit
			int cardCenterX = x + (CARD_WIDTH / 2);
			int cardCenterY = y + (CARD_HEIGHT / 2);

			Image suit = null;
			if (c.getSuit().equals(Card.SUIT.DIAMONDS)) {
				suit = diamond;
			} else if (c.getSuit().equals(Card.SUIT.CLUBS)) {
				suit = club;
			} else if (c.getSuit().equals(Card.SUIT.HEARTS)) {
				suit = heart;
			} else if (c.getSuit().equals(Card.SUIT.SPADES)) {
				suit = spade;
			}

			g.drawImage(suit, cardCenterX - 25, cardCenterY - 25, 50, 50, null);

			// draw border
			g.drawRoundRect(x, y, CARD_WIDTH, CARD_HEIGHT, BORDER_RADIUS, BORDER_RADIUS);
		} else {
			g.drawImage(back, x , y, CARD_WIDTH, CARD_HEIGHT, null);
			g.setColor(Color.BLACK);
		}
	}

	private int getScore(List<Card> cards) {
		int score = 0;
		int acesFound = 0;

		// count non-ace cards
		for(Card c : cards) {
			if (c.isFaceUp()) {
				if (c.getIntValue() > 10) {
					score += 10;
				} else if (c.getIntValue() > 1){
					score += c.getIntValue();
				} else {
					acesFound++;
				}
			}
		}

		// now count aces
		if (acesFound > 0) {
			if (acesFound == 1) {
				if (score <= 10) {
					score += 11;
				} else {
					score += 1;
				}
			} else if (acesFound == 2) {
				if (score + 12 < 21) {
					score += 12;
				} else {
					score += 2;
				}
			} else if (acesFound == 3) {
				if (score + 13 < 21) {
					score += 13;
				} else {
					score += 3;
				}
			} else {
				if (score + 14 < 21) {
					score += 14;
				} else {
					score += 4;
				}
			}
		}
		return score;
	}

	private void dealerTurn() {
		Runnable r = new Runnable() {
			public void run() {
				try {
					Thread.sleep(500);
					flipCard(dealerPlayedCards.get(1));
					repaint();

					int dealerScore = getScore(dealerPlayedCards);
					int playerScore = getScore(playerPlayedCards);

					while (dealerScore < 21 && dealerScore <= playerScore && playerScore < 21) {
						Thread.sleep(500);
						dealerPlayedCards.add(deck.getTopCard());
						flipCard(dealerPlayedCards.get(dealerPlayedCards.size() - 1));
						dealerScore = getScore(dealerPlayedCards);

						repaint();	
					}

					roundFinished = true;

					if (didPlayerWin()) {
						money += WIN_PER_GAME;
					}
					repaint();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};

		Thread t = new Thread(r);
		t.start();
	}

	private void flipCard(Card c) {
		playAudio("res/flip.wav");
		c.flip();
	}

	private boolean didPlayerWin() {
		int playerScore = getScore(playerPlayedCards);
		int dealerScore = getScore(dealerPlayedCards);
		boolean playerWon = false;

		if (playerScore == 21) {
			playerWon = true;
		} else if (dealerScore > 21) {
			playerWon = true;
		} else {
			playerWon = (playerScore > dealerScore) && playerScore <= 21;
		}
		return playerWon;
	}
}
