package com.talixa.games.blackjack.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {

	private List<Card> deck;

	public Deck() {
		deck = new ArrayList<Card>();

		for(int i = 0; i < 4; ++i) {
			Card.SUIT s = Card.SUIT.CLUBS;
			switch (i) {
				case 0: s = Card.SUIT.CLUBS; break;
				case 1: s = Card.SUIT.DIAMONDS; break; 
				case 2: s = Card.SUIT.HEARTS; break;
				case 3: s = Card.SUIT.SPADES; break;
				default: s = Card.SUIT.SPADES; break;
			}

			for(int j = 0; j < 13; ++j) {
				deck.add(new Card(s, j + 1));
			}
		}
	}

	public void shuffle() {
		Collections.shuffle(deck);
	}

	public Card getTopCard() {
		return deck.remove(0);
	}

	public static void main(String args[]) {
		Deck d = new Deck();
		d.shuffle();
		for(int i = 0; i < 7; ++i) {
			System.out.println(d.getTopCard());
		}
	}
}
