package com.talixa.games.blackjack;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import com.talixa.games.blackjack.frames.FrameAbout;
import com.talixa.games.blackjack.listeners.DefaultWindowListener;
import com.talixa.games.blackjack.listeners.ExitActionListener;
import com.talixa.games.blackjack.shared.IconHelper;
import com.talixa.games.blackjack.shared.BlackJackConstants;
import com.talixa.games.blackjack.widgets.CardPanel;

public class BlackJack {
	private static JFrame frame;
	private static CardPanel cardPanel;
	private static JCheckBoxMenuItem soundEnabledMenuItem;

	private static void createAndShowGUI() {
		frame = new JFrame(BlackJackConstants.TITLE_MAIN);

		// set close functionality 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addWindowListener(new DefaultWindowListener());

		cardPanel = new CardPanel();
		frame.add(cardPanel);

		// set icon
		IconHelper.setIcon(frame);

		// add menus
		addMenus();

		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(BlackJackConstants.APP_WIDTH, BlackJackConstants.APP_HEIGHT));
		int left = (screenSize.width / 2) - (BlackJackConstants.APP_WIDTH / 2);
		int top  = (screenSize.height / 2) - (BlackJackConstants.APP_HEIGHT / 2);
		frame.pack();
		frame.setLocation(left, top);
		frame.setVisible(true);
	}

	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(BlackJackConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);

		JMenuItem newGameItem = new JMenuItem(BlackJackConstants.MENU_NEW);
		newGameItem.setMnemonic(KeyEvent.VK_N);
		newGameItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				cardPanel.reset(false);
			}
		});
		fileMenu.add(newGameItem);

		JMenuItem exitMenuItem = new JMenuItem(BlackJackConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		fileMenu.add(exitMenuItem);

		//*******************************************************************************
		// Sound menu
		JMenu soundMenu = new JMenu(BlackJackConstants.MENU_SOUND);
		soundMenu.setMnemonic(KeyEvent.VK_S);
		soundEnabledMenuItem = new JCheckBoxMenuItem(BlackJackConstants.MENU_ENABLED);
		soundEnabledMenuItem.setMnemonic(KeyEvent.VK_E);
		soundEnabledMenuItem.setSelected(true);
		soundEnabledMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardPanel.enableSound(soundEnabledMenuItem.isSelected());
			}
		});
		soundMenu.add(soundEnabledMenuItem);

		//*******************************************************************************
		// Setup help menu
		JMenu helpMenu = new JMenu(BlackJackConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		JMenuItem aboutMenuItem = new JMenuItem(BlackJackConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);
			}
		});
		helpMenu.add(aboutMenuItem);

		//*******************************************************************************
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);	
		menuBar.add(soundMenu);
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
