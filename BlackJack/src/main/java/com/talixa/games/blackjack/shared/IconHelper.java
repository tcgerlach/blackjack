package com.talixa.games.blackjack.shared;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;
import java.io.IOException;

import javax.imageio.ImageIO;

public class IconHelper {

	public static void setIcon(Window w) {
		ClassLoader cl = IconHelper.class.getClassLoader();	
		Image im = Toolkit.getDefaultToolkit().getImage(cl.getResource(BlackJackConstants.ICON));
		w.setIconImage(im);
	}
	
	public static Image getImage(String name) {			
		ClassLoader cl = IconHelper.class.getClassLoader();
		Image im = null;
		try {
			im =  ImageIO.read(cl.getResource(name));
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		return im;
	}
	
	private IconHelper() {
		// private constructor
	}
}