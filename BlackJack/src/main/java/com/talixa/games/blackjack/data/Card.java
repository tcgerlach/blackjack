package com.talixa.games.blackjack.data;

public class Card {

	public enum SUIT {
		DIAMONDS, HEARTS, CLUBS, SPADES
	};

	private SUIT suit;
	private int value;
	private boolean faceUp;

	public Card(SUIT suit, int value) {
		this.suit = suit;
		this.value = value;
		faceUp = false;
	}

	public boolean isRed() {
		return suit.equals(SUIT.DIAMONDS) || suit.equals(SUIT.HEARTS);
	}

	public boolean isBlack() {
		return !isRed();
	}

	public SUIT getSuit() {
		return this.suit;
	}

	public int getIntValue() {
		return value;
	}

	public boolean isFaceUp() {
		return faceUp;
	}

	public void flip() {
		faceUp = !faceUp;
	}

	public String getValue() {
		String valueName = "";
		if (value == 1) {
			valueName = "Ace";
		} else if (value < 11) {
			valueName = String.valueOf(value); 
		} else if (value == 11) {
			valueName = "Jack";
		} else if (value == 12) {
			valueName = "Queen";
		} else if (value == 13) {
			valueName = "King";
		}

		return valueName;
	}

	public String getAbbreviatedValue() {
		String valueName = "";
		if (value == 1) {
			valueName = "A";
		} else if (value < 11) {
			valueName = String.valueOf(value);
		} else if (value == 11) {
			valueName = "J";
		} else if (value == 12) {
			valueName = "Q";
		} else if (value == 13) {
			valueName = "K";
		}

		return valueName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((suit == null) ? 0 : suit.hashCode());
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (suit != other.suit)
			return false;
		if (value != other.value)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getValue() + " " + suit.toString();
	}
}
